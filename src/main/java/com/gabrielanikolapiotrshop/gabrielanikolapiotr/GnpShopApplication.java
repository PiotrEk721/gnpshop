package com.gabrielanikolapiotrshop.gabrielanikolapiotr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GnpShopApplication {

    public static void main(String[] args) {
        SpringApplication.run(GnpShopApplication.class, args);
    }

}
